#!/bin/bash

# Répertoire source à sauvegarder (votre répertoire personnel, par exemple)
source_directory="$HOME/mon_repertoire"

# Récupère l'heure actuelle sous forme d'entier (HHMM)
current_time=$(date +'%H%M')

# Répertoire de sauvegarde avec la date actuelle comme nom de dossier
backup_directory="$HOME/sauvegarde_$(date +'%Y%m%d_%H%M%S')"

# Créez le répertoire de sauvegarde
mkdir -p "$backup_directory"

# Effectuez la sauvegarde en copiant les fichiers du répertoire source vers le répertoire de sauvegarde
cp -r "$source_directory"/* "$backup_directory"

# Si l'heure actuelle est minuit (00:00), affichez un message supplémentaire pour indiquer la sauvegarde à minuit
if [ "$current_time" = "0000" ]; then
    echo "Sauvegarde à minuit terminée dans : $backup_directory"
else
    echo "Sauvegarde terminée dans : $backup_directory"
fi

#************************************ FAIT A l'AIDE DE CHAT GPT *************************************
